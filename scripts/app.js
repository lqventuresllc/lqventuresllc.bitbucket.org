(function(){
    'use strict';

    angular.module('lqventuresllc',['ngRoute'])
        .config(['$routeProvider',function($routeProvider){
            $routeProvider
            .when('/',{
                controller: 'homeController',
                templateUrl: 'views/home.html'
            })
            .when('/creedsthoughts',{
                controller: 'creedsThoughtsController',
                templateUrl: 'views/creedsthoughts.html'
            })
        }]);
})();