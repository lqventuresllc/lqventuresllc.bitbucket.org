(function(){
    'use strict';

    angular.module('lqventuresllc').controller('creedsThoughtsController',['$scope',function($scope){
        $scope.project_name = 'Creed\'s Thoughts';
        $scope.app_link = '../app/CreedsThoughtsSetup.msi';
    }]);
})();