(function(){
    'use strict';

    angular.module('lqventuresllc').controller('homeController',['$scope',function($scope){
        $scope.message = "Welcome to LQ Ventures, LLC Project Page";
        $scope.links = [
            {
                name: 'Creed\'s Thoughts',
                url: '#/creedsthoughts' 
            },
            {
                name: 'Project Manager',
                url: '#/projectmanager'
            }
        ];
    }]);
})();